const app = require('express')()
const http = require('http').createServer(app)
const io = require('socket.io')(http, {
    cors: {
        origin: true
    }
})

require('dotenv').config()

const port = process.env.PORT || 3000

io.on('connection', (socket) => {
    console.log('a user connected')

    socket.on('send-message', (msg) => {
        console.log(`Get new message: ${msg}, from ${socket.id}`)
    })

    socket.on('disconnect', () => {
        console.log('user disconnected')
    })
})

http.listen(port, () => {
    console.log(`~ App listening at port ${port}...`)
})